<?php

require_once __DIR__ . '/Database.php';

/**
 *
 */
class ArtistQuery extends Database
{

    function __construct()
    {
        # code...
        parent::__construct();
    }

    function getAll(){
        $sql = "SELECT * FROM artists ORDER BY artist_name ASC";
        $statement = static::$pdo->prepare($sql);
        $statement->execute();
        $results = $statement->fetchAll(PDO::FETCH_OBJ);
        return $results;
    }
}


?>
