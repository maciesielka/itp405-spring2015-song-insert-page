<?php

require_once __DIR__ . '/ArtistQuery.php';
require_once __DIR__ . '/GenreQuery.php';
require_once __DIR__ . '/Song.php';

if(isset($_POST['submit'])){
    $title = $_POST["title"];
    $artist_id = $_POST["artist_id"];
    $genre_id = $_POST["genre_id"];
    $price = $_POST["price"];

    $song = new Song();
    $song->setTitle($title);
    $song->setArtistId($artist_id);
    $song->setGenreId($genre_id);
    $song->setPrice($price);
    $song->save();
}

?>


<!DOCTYPE html>
<html>
    <head>
        <title>Song Insert Page | Michael Ciesielka</title>
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
        <style>
            .content {
                margin-top: 100px;
                font-size: 20px;
            }

            .title {
                margin-bottom: 50px;
            }

            .feedback {
                color: red;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content text-center">
                <div class="title text-center"><h1>Add a New Song!</h1></div>
                <form action="#" method="post">
                    <input type="text" name="title" placeholder="Enter title here...">
                    <select name="artist_id">
                        <option selected disabled>Select an Artist</option>
                        <?php $artist_query = new ArtistQuery(); ?>
                        <?php foreach($artist_query->getAll() as $artist) : ?>
                            <option value="<?php echo $artist->id ?>"><?php echo $artist->artist_name ?></option>
                        <?php endforeach; ?>
                    </select>
                    <select name="genre_id">
                        <option selected disabled>Select a Genre</option>
                        <?php $genre_query = new GenreQuery(); ?>
                        <?php foreach($genre_query->getAll() as $genre) : ?>
                            <option value="<?php echo $genre->id ?>"><?php echo $genre->genre ?></option>
                        <?php endforeach; ?>
                    </select>
                    <input type="text" name="price" placeholder="Price">
                    <input type="submit" name="submit" value="Submit">
                </form>
            </div>
            <?php if(isset($_POST['submit'])) : ?>
            <div class="text-center">
                <p class="feedback">The song <?php echo $song->getTitle() ?> with an ID of <?php echo $song->getId() ?> was inserted successfully!</p>
                <?php endif; ?>
            </div>
        </div>
    </body>
</html>
