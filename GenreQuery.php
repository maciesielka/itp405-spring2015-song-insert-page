<?php

require_once __DIR__ . '/Database.php';

/**
 *
 */
class GenreQuery extends Database
{

    function __construct()
    {
        # code...
        parent::__construct();
    }

    function getAll(){
        $sql = "SELECT * FROM genres ORDER BY genre ASC";
        $statement = static::$pdo->prepare($sql);
        $statement->execute();
        $results = $statement->fetchAll(PDO::FETCH_OBJ);
        return $results;
    }
}


?>
